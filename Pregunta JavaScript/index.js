const mysql = require('mysql');
const connection = mysql.createConnection({
host: "localhost",
user: "root",
password: "",
database: "pregunta",});
connection.connect((error)=>{
if(error) {
console.log('Error de conexión : '+error);
return;}
console.log('Conexión exitosa a la base de datos!');});

const express = require('express');
const programa = express();
programa.use(express.urlencoded({extended:false}));
programa.use(express.json());
programa.set('view engine', 'ejs');

const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');

programa.get("/", (req, res) => {
    res.render("registro"); });

//6 .- Metodo
programa.post("/", async (req, res) => {
    const {cedula, nombres, direccion, telefono, correo} = req.body
    const expresiones = {
        cedula: /^\d{10}$/, // Numérico de 10 dígitos.
        nombres: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, //Texto de al menos 50 digitos.
        direccion: /^([a-zA-Z0-9_-]){1,60}$/, //Alfanumérico de al menos 60 digitos.
        telefono: /^\d{10}$/, // Numérico de 10 dígito.
        correo: /\S+@\S+.\S+/, // Formato válido de correo.
    };

    if(!expresiones.cedula.test(cedula)){
        res.json({'error': 'La cédula debe tener 10 digitos'})
    }else  if(!expresiones.nombres.test(nombres)){
        res.json({'error': 'Ingrese nombre y apellido / MAX= 50 caracteres'})
    }else if(!expresiones.direccion.test(direccion)){
        res.json({'error': 'Ingrese una dirección válida / MAX= 60 caracteres'})
    }else if(!expresiones.telefono.test(telefono)){
        res.json({'error': 'El telefono debe ser numérico de 10 dígitos'})
    }else if(!expresiones.correo.test(correo)){
        res.json({'error': 'El correo debe estar en un formato válido'})
    }else{
        try {
            await pool.query(
                "INSERT INTO estudiante(cedula, nombres, direccion, telefono, correo) VALUES($1, $2, $3, $4, $5) RETURNING *", 
                [cedula, nombres, direccion, telefono, correo],
            (error, result) =>{
                    if(error){
                        res.json({'error': error})
                    }else{
                        res.json(result.rows)
                    }
                }
            );
        } catch (error) {
            res.json({'error': error})
        }
    }
});

programa.listen(3000, (req, res) => {
    console.log("SERVER RUNNING IN http://localhost:3000");
  });